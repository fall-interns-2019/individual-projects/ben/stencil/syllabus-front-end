import { Component, State, h } from '@stencil/core';

@Component({
  tag: 'my-profile',
  styleUrl: 'my-profile.css'
})
export class MyProfile {

  userEndPoint = 'http://localhost:3000/api/v1/users';
  @State() userData: any;

  async componentWillLoad() {
    await this.fetchProfile();
  }

  async fetchProfile() {
    const res = await fetch(this.userEndPoint + '/' + localStorage.user_id);
      let profileInfo = await res.json();
      const {id, first_name, last_name, email} = profileInfo;
      console.log(profileInfo);
      this.userData = {
        id,
        first_name,
        last_name,
        email
      }

  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>My Profile</ion-title>
          <ion-buttons slot="primary">
            <ion-button href = {`/editprofile/${this.userData.id}`}>Edit</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,

      <ion-card>
        <ion-row  style={{paddingTop: '5px'}}align-items-center justify-content-around>
          <ion-icon  style={{fontSize: '4rem'}} name="contact" color={'indigo'}> </ion-icon>
        </ion-row>
        <ion-card-content>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-label>First Name</ion-label>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-text style={{fontWeight:'bolder', fontSize:'1.5rem'}}>{this.userData.first_name}</ion-text>
          </ion-row><ion-row>
          <ion-label>Last Name</ion-label>
        </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-text style={{fontWeight:'bolder', fontSize:'1.5rem'}}>{this.userData.last_name}</ion-text>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-label>Email</ion-label>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-text style={{fontWeight:'bolder', fontSize:'1.5rem'}}>{this.userData.email}</ion-text>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-item lines='inset'>
            <ion-label>Communication Settings</ion-label>
            </ion-item>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-icon size='large' name="call"></ion-icon>
          </ion-row>
        </ion-card-content>
      </ion-card>,
      <app-footer />
    ];
  }
}
