import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-registration',
  styleUrl: 'app-registration.css'
})

export class AppRegistration {
  userEndPoint = 'http://localhost:3000/api/v1/users';
  email: HTMLIonInputElement;
  password: HTMLIonInputElement;
  first_name: HTMLIonInputElement;
  last_name: HTMLIonInputElement;

  getSignUpInfo() {
    let userInput = {
      'email': this.email.value,
      'password': this.password.value,
      'first_name': this.first_name.value,
      'last_name': this.last_name.value
    };
    this.postSignUpInfo(userInput);
  }

  async postSignUpInfo(userInput) {
    const res = await fetch(this.userEndPoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(userInput)
    });
    const json = await res.json();
    localStorage.setItem('user_id', json.id);
    console.log(json);
    window.location.href = '/profile'
  }
  render() {
    return [
      <ion-content color="indigo">
        <ion-grid>
              <ion-row style={{marginTop:'105px'}} align-items-center justify-content-around>
              <form>
                <ion-col>
                  <ion-item color="indigo">
                    <ion-label style={{fontSize: '1.2rem'}} color="warning" position="stacked">First Name</ion-label>
                    <ion-input ref={(el) => this.first_name = el as HTMLIonInputElement} style={{fontSize: '1.5rem'}}  id="input-first-name" required type="text"
                               />
                  </ion-item>
                  <ion-item color="indigo" >
                    <ion-label style={{fontSize: '1.2rem'}} color="warning" position="stacked">Last Name</ion-label>
                    <ion-input ref={(el) => this.last_name = el as HTMLIonInputElement} style={{fontSize: '1.5rem'}} id="input-last-name" required type="text"
                               />
                  </ion-item>
                  <ion-item color="indigo" >
                    <ion-input ref={(el) => this.email = el as HTMLIonInputElement} style={{fontSize: '1.5rem'}} id="input-email" required type="email" placeholder="Email"
                               />
                  </ion-item>
                  <ion-item color="indigo">
                    <ion-input ref={(el) => this.password = el as HTMLIonInputElement} style={{fontSize: '1.5rem'}} id="input-password" required type="password" placeholder="Password"
                              />
                  </ion-item>
                  <ion-button onClick={() => this.getSignUpInfo()}  style={{marginTop: '18px'}}color='warning' id='registrationButton' expand="full">Create Account</ion-button>
                </ion-col>
              </form>
              </ion-row>
                <ion-row style={{marginTop: "10px"}} align-items-center justify-content-around>
                <ion-label className="ion-text-center center-horiz">Already have an account?</ion-label>
                </ion-row>
                <ion-row align-items-center justify-content-around>
                  <ion-button href='/' fill='clear'>SIGN IN</ion-button>
                </ion-row>
        </ion-grid>
      </ion-content>


    ];
  }
}
