import { Component, h } from '@stencil/core';

@Component({
  tag: 'assignment-card',
  styleUrl: 'assignment-card.css'
})
export class AssignmentCard {
  courseEndPoint = 'http://localhost:3000/api/v1/courses';
  assignments: any[];
  courses: any[];

  async componentWillLoad() {
    await this.fetchAssignmentData();
  }

  async fetchAssignmentData() {
    let params = window.location.href.split('/');
    let course_id = params[params.length - 1];
    const res = await fetch(this.courseEndPoint + '/' + course_id);
    let course = await res.json();
    this.courses = course;
    this.assignments = course.assignments;
  }



  renderAssignments() {
    return this.assignments.map((assignment) => {
      return [
        <ion-label style={{marginLeft:'1rem', fontFamily:'Helvetica-Neue', fontWeight:'bold'}}>{assignment.due_date}</ion-label>,
          <ion-card>
            <ion-item lines='full' slot={'start'}><ion-label>Business Admin</ion-label></ion-item>,
              <ion-row>
              <ion-toolbar>
              <ion-label style={{paddingLeft:'1rem'}} slot={'start'}>{assignment.name}</ion-label>
              <ion-label style={{paddingRight:'1rem'}} slot={'end'}>{assignment.points} pts</ion-label>
              </ion-toolbar>
              </ion-row>,
            <ion-card-content>
              {assignment.description}
            </ion-card-content>
          </ion-card>
      ]
    })
  }

  render() {
    return [
      <ion-content fullscreen>
        {this.renderAssignments()}
      </ion-content>
    ];
  }
}
