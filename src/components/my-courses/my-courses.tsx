import { Component, h } from '@stencil/core';

@Component({
  tag: 'my-courses',
  styleUrl: 'my-courses.css'
})
export class MyCourses {
  userEndPoint = 'http://localhost:3000/api/v1/users';
  courses: any[];

  async componentWillLoad() {
    const res = await fetch(this.userEndPoint + '/' + localStorage.user_id);
    let user =  await res.json();
    this.courses = user.courses;
  }

  emptyCoursesOrNah() {
    if (this.courses.length > 0) {
      return <my-courses-card />
    }
    else {
      return <no-courses/>
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar style={{marginBottom:'2rem'}}>
          <ion-title>My Courses</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        {this.emptyCoursesOrNah()},
      </ion-content>,
      <app-footer />

    ];
  }
}
