import {Component, h, Prop} from '@stencil/core';
import {CrudService} from "../../services/crud.service";

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {
@Prop() CrudService = new CrudService();
   static getCrudService() {
    return document.querySelector('app-root').CrudService
   }
  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="app-home" />
          <ion-route url="/colorpicker" component="color-picker" />
          <ion-route url="/register" component="app-registration" />
          <ion-route url="/profile/" component="my-profile" />
          <ion-route url="/editprofile/:id" component="edit-profile" />
          <ion-route url="/addcourse" component="add-course" />
          <ion-route url="/courseinfo/:id" component="course-info" />
          <ion-route url="/editcourseinfo/:id" component="edit-course-info" />
          <ion-route url="/assignments" component="empty-assignments" />
          <ion-route url="/addassignment/:course_id" component="add-assignment" />
          <ion-route url="/myassignments/" component="my-assignments" />
          <ion-route url="/mycourses" component="my-courses" />
          <ion-route url="/syllabusupload" component="syllabus-upload" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
