import { Component, h } from '@stencil/core';

@Component({
  tag: 'syllabus-upload',
  styleUrl: 'syllabus-upload.css'
})
export class SyllabusUpload {

  render() {
    return [

      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="secondary">
            <ion-button href={'/mycourses'}>Back</ion-button>
          </ion-buttons>
          <ion-title>Syllabus Upload</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-row>
        <ion-col>
          <img id='addAssignment' src='../assets/icon/robot.svg' style={{textAlign: 'center', maxWidth:'80%', display:'block', margin:'auto'}}/>
        </ion-col>
      </ion-row>,
      <ion-row>
        <ion-col style={{textAlign:'center'}}>
          <ion-text style={{fontSize:'1.2rem', fontWeight:'bold'}}>
            Use your camera to capture your syllabus and one of our robot elves will input your assignments for you.
          </ion-text>
        </ion-col>
      </ion-row>,
      <ion-row style={{marginBottom:'3rem'}}>
        <ion-col style={{textAlign:'center'}}>
          <label for="file-upload" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i> Upload Syllabus
          </label>
          <input id="file-upload" type="file"/>
        </ion-col>
      </ion-row>
    ];
  }
}
