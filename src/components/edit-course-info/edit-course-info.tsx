import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'edit-course-info',
  styleUrl: 'edit-course-info.css'
})
export class EditCourseInfo {
  @Prop() course: {id: number, name:string, number:string, section:string, term:string, instructor:string};
  address = 'http://localhost:3000/api/v1/courses/';
  name: HTMLIonInputElement;
  number: HTMLIonInputElement;
  section: HTMLIonInputElement;
  term: HTMLIonInputElement;
  instructor: HTMLIonInputElement;

  async componentWillLoad() {
    let params = window.location.href.split('/');
    let id = params[params.length - 1];
    const res = await fetch(this.address + id);
    this.course = await res.json();
  }

  async getEditedInfo() {
    let userInput = {
      'name': this.name.value,
      'number': this.number.value,
      'section': this.section.value,
      'term': this.term.value,
      'instructor': this.instructor.value
    };
    this.putFetchEditedInfo(userInput);
  }

  async putFetchEditedInfo(userInput) {
    const res = await fetch(this.address + this.course.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(userInput)
    });
    const editedInfo = await res.json();
    console.log('inside edit info', editedInfo);
    window.location.href = '/mycourses';
  }

  async deleteCourse() {
    await fetch(this.address + this.course.id, {
      method: 'DELETE'
    });
    window.location.href = '/mycourses';
  }


  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="secondary">
            <ion-button href={'/mycourses'}>Cancel</ion-button>
          </ion-buttons>
          <ion-title>Course Information</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-card style={{marginTop: '5px'}}>
          <ion-row  style={{paddingTop: '5px'}} align-items-center justify-content-around>
            <ion-icon  style={{fontSize: '4rem'}} name="contact" color={'indigo'}> </ion-icon>
          </ion-row>
          <ion-card-content>
            <ion-row>
              <ion-label >Course Name</ion-label>
            </ion-row>
            <ion-row>
              <ion-input ref={(el) => this.name = el as HTMLIonInputElement} value={this.course.name} style={{fontWeight:'bolder', fontSize:'1.2rem'}}/>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Course Number</ion-label>
            </ion-row>
            <ion-row>
              <ion-input ref={(el) => this.number = el as HTMLIonInputElement} value={this.course.number} style={{fontWeight:'bolder', fontSize:'1.2rem'}}/>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Section Number</ion-label>
            </ion-row>
            <ion-row>
              <ion-input ref={(el) => this.section = el as HTMLIonInputElement} value={this.course.section} style={{fontWeight:'bolder', fontSize:'1.2rem'}}/>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Term</ion-label>
            </ion-row>
            <ion-row>
              <ion-input ref={(el) => this.term = el as HTMLIonInputElement} value={this.course.term} style={{fontWeight:'bolder', fontSize:'1.2rem'}}/>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Instructor</ion-label>
            </ion-row>
            <ion-row>
              <ion-input ref={(el) => this.instructor = el as HTMLIonInputElement} value={this.course.instructor} style={{fontWeight:'bolder', fontSize:'1.2rem'}}/>
            </ion-row>
            <section>
              <ion-button onClick={() => this.getEditedInfo()} color='warning' expand={'full'}>SAVE CHANGES</ion-button>
            </section>
            <section>
              <ion-button onClick={() => this.deleteCourse()} color='danger' expand={'full'}>DELETE COURSE</ion-button>
            </section>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ];
  }
}
