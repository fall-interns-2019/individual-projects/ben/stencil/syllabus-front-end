import { Component, h } from '@stencil/core';

@Component({
  tag: 'empty-assignments',
  styleUrl: 'empty-assignments.css'
})
export class EmptyAssignments {

  render() {
    return [

      <ion-header style={{fontSize: '1.7rem', fontWeight: 'lighter', textAlign:'center', paddingTop: '9px'}}>Assignments</ion-header>,
      <ion-row>
        <ion-col>
          <img id='addAssignment' src='../assets/icon/robot.svg' style={{textAlign: 'center'}}/>
        </ion-col>
      </ion-row>,
      <ion-row>
        <ion-col style={{textAlign:'center'}}>
          <ion-text style={{fontSize:'1.2rem', fontWeight:'bold'}}>
            Our robots are working hard uploading other assignments. Would you like to manually upload them?
          </ion-text>
        </ion-col>
      </ion-row>,
      <ion-row style={{marginBottom:'3rem'}}>
        <ion-col style={{textAlign:'center'}}>
          <a href={'/addassignment'}><ion-label style={{color:'indigo'}}>INPUT MANUAL ASSIGNMENTS</ion-label></a>
        </ion-col>
      </ion-row>
    ];
  }
}
