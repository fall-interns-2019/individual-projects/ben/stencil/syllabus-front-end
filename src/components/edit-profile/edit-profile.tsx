import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'edit-profile',
  styleUrl: 'edit-profile.css'
})
export class EditProfile {
  @Prop() user: {id: number, first_name:string, last_name:string, email:string};

  userEndPoint = 'http://localhost:3000/api/v1/users/';

  email: HTMLIonInputElement;
  first_name: HTMLIonInputElement;
  last_name: HTMLIonInputElement;

  async componentWillLoad() {
    let params = window.location.href.split('/');
    let id = params[params.length - 1];
    const res = await fetch(this.userEndPoint + id);
    this.user = await res.json();
    console.log(this.user)
  }

  async getEditedInfo() {
    let userInput = {
      'email': this.email.value,
      'first_name': this.first_name.value,
      'last_name': this.last_name.value
    };
    this.putFetchEditedInfo(userInput);
  }

  async putFetchEditedInfo(userInput) {
    const res = await fetch(this.userEndPoint + this.user.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(userInput)
    });
    const editedInfo = await res.json();
    console.log('inside edit info', editedInfo);
    window.location.href = '/profile'
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Edit Profile</ion-title>
          <ion-buttons slot="primary">
            <ion-button href={'/profile'}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,

      <ion-card>
        <ion-row style={{paddingTop: '1px'}} align-items-center justify-content-around>
          <ion-icon style={{fontSize: '4rem'}} name="contact" color={'indigo'}> </ion-icon>
        </ion-row>
        <ion-card-content>
          <ion-row style={{paddingTop: '10px'}}>
            <ion-label>First Name</ion-label>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-input ref={(el) => this.first_name = el as HTMLIonInputElement} value={this.user.first_name} style={{fontWeight:'bolder', fontSize:'1.5rem'}}/>
          </ion-row><ion-row>
          <ion-label>Last Name</ion-label>
        </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-input ref={(el) => this.last_name = el as HTMLIonInputElement} value={this.user.last_name} style={{fontWeight:'bolder', fontSize:'1.5rem'}}/>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-label>Email</ion-label>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-input ref={(el) => this.email = el as HTMLIonInputElement} value={this.user.email} style={{fontWeight:'bolder', fontSize:'1.5rem'}}/>
          </ion-row>
          <ion-row style={{paddingTop: '10px'}}>
            <ion-item lines='inset'>
              <ion-label>Communication Settings</ion-label>
            </ion-item>
          </ion-row>
          <ion-row style={{paddingTop: '20px'}}>
            <ion-list>
            <ion-item>
              <ion-label>Phone</ion-label>
              <ion-toggle slot="end" name="phone" checked color={'success'}></ion-toggle>
            </ion-item>
              <ion-item>
                <ion-label>Email</ion-label>
                <ion-toggle slot="end" name="email" unchecked color={'success'}></ion-toggle>
              </ion-item>
            </ion-list>
          </ion-row>
          <section>
            <ion-button onClick={() => this.getEditedInfo()} color='warning' expand={'full'}>SAVE CHANGES</ion-button>
          </section>
        </ion-card-content>
      </ion-card>
    ];
  }
}
