import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'course-info',
  styleUrl: 'course-info.css'
})
export class CourseInfo {
  @Prop() course: {id: number, name:string, number:string, section:string, term:string, instructor:string};
  address = 'http://localhost:3000/api/v1/courses/'

  async componentWillLoad() {
    let params = window.location.href.split('/');
    let id = params[params.length - 1];
    const res = await fetch(this.address + id);
    this.course = await res.json();
    console.log(this.course)
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="secondary">
            <ion-button href={'/mycourses'}>Back</ion-button>
          </ion-buttons>
          <ion-title>Course Information</ion-title>
          <ion-buttons slot="primary">
            <ion-button href = {`/editcourseinfo/${this.course.id}`}>Edit</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-card style={{marginTop: '5px'}}>
          <ion-row  style={{paddingTop: '5px'}} align-items-center justify-content-around>
            <ion-icon  style={{fontSize: '4rem'}} name="contact" color={'indigo'}> </ion-icon>
          </ion-row>
          <ion-card-content>
            <ion-row>
              <ion-label >Course Name</ion-label>
            </ion-row>
            <ion-row>
              <ion-text style={{fontWeight:'bolder', fontSize:'1.2rem'}}>{this.course.name}</ion-text>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Course Number</ion-label>
            </ion-row>
            <ion-row>
              <ion-text style={{fontWeight:'bolder', fontSize:'1.2rem'}}>{this.course.number}</ion-text>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Section Number</ion-label>
            </ion-row>
            <ion-row>
              <ion-text style={{fontWeight:'bolder', fontSize:'1.2rem'}}>{this.course.section}</ion-text>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Term</ion-label>
            </ion-row>
            <ion-row>
              <ion-text style={{fontWeight:'bolder', fontSize:'1.2rem'}}>{this.course.term}</ion-text>
            </ion-row>
            <ion-row style={{marginTop:'1.5rem'}}>
              <ion-label>Instructor</ion-label>
            </ion-row>
            <ion-row>
              <ion-text style={{fontWeight:'bolder', fontSize:'1.2rem'}}>{this.course.instructor}</ion-text>
            </ion-row>
            <ion-row style={{marginTop:'2rem'}}>
              <ion-toolbar>
                <ion-list>
                  <ion-item-divider style={{background: 'transparent', paddingLeft: '1px'}}>
                    <ion-icon size='large' slot={'start'} name={'document'} style={{color:'skyblue'}}></ion-icon>
                    <ion-label style={{paddingLeft: '1rem'}}>Syllabus Uploads</ion-label>
                    <ion-label slot={'end'}>0</ion-label>
                  </ion-item-divider>
                </ion-list>
              </ion-toolbar>
            </ion-row>
            <ion-row>
              <ion-toolbar>
                <ion-list>
                  <ion-item-divider style={{background: 'transparent', paddingLeft: '1px'}}>
                    <ion-icon size='large' slot={'start'} name={'paper'} style={{color:'skyblue'}}></ion-icon>
                    <ion-label style={{paddingLeft: '1rem'}}>Assignments</ion-label>
                    <ion-label slot={'end'}>0</ion-label>
                  </ion-item-divider>
                </ion-list>
              </ion-toolbar>
            </ion-row>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ];
  }
}
//
// <ion-grid>
//   <ion-row>
//     <ion-col col-2 >
//     </ion-col>
//     <ion-col col-8 style={{textAlign:'center'}}>
//       <ion-header style={{fontSize: '1.2rem', fontWeight: 'lighter', textAlign:'center', paddingTop: '20px', display: 'inline-block'}}>Course Information</ion-header>
//     </ion-col>
//     <ion-col col-2 >
//       <ion-button href='/mycourses/' style={{float: 'right'}} fill={'clear'}>Edit</ion-button>
//     </ion-col>
//   </ion-row>
// </ion-grid>
