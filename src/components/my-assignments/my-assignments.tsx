import { Component, h } from '@stencil/core';

@Component({
  tag: 'my-assignments',
  styleUrl: 'my-assignments.css'
})
export class MyAssignments {

  render() {
    return [
      <ion-header>
        <ion-toolbar style={{marginBottom:'2rem'}}>
          <ion-title>Assignments</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <assignment-card />
      </ion-content>,
      <ion-content>
        <a href={'/addassignment'}><img id='addAssignment' src={'../assets/icon/BTN_AddAssignment.svg'} /></a>
      </ion-content>,
      <app-footer />
    ];
  }
}
