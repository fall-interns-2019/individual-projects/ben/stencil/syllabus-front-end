import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-login',
  styleUrl: 'app-login.css'
})
export class AppLogin {

  userEndPoint = 'http://localhost:3000/api/v1/users';
  email: HTMLIonInputElement;
  password: HTMLIonInputElement;

   getLoginInfo() {
    let userInput = {
      'email': this.email.value,
      'password': this.password.value
    }
    this.postUserData(userInput)
  }

  async postUserData(userInput) {
    const res = await fetch(this.userEndPoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(userInput)
    });
    const json = await res.json();
    console.log(json)
  }

  render() {
    return [
      <ion-content color='indigo'>
        <ion-grid>
          <ion-row style={{marginTop: '6%'}}>
            <img id='login' src='../assets/icon/Sillibi_Horz-Logo.svg' alt={'logo'}/>
          </ion-row>
          <form>
            <ion-col>
                <ion-item color='indigo' style={{marginLeft: '3rem', marginRight:'3rem'}}>
                  <ion-input ref={(el) => this.email = el as HTMLIonInputElement} type='email' placeholder='Email Address'/>
                </ion-item>
                <ion-item color='indigo' style={{marginLeft: '3rem', marginRight:'3rem'}}>
                  <ion-input ref={(el) => this.password = el as HTMLIonInputElement} type='password' placeholder='Password'/>
                </ion-item>
                <ion-button color='warning' onClick={() => this.getLoginInfo()} id='loginButton' expand="full" style={{marginLeft: '3rem', marginRight:'3rem'}}>LOGIN</ion-button>
                <ion-button color='facebook' id='facebookButton' expand="full" style={{marginLeft: '3rem', marginRight:'3rem'}}>LOGIN WITH FACEBOOK</ion-button>
            </ion-col>
          </form>
          <ion-row align-items-center justify-content-around>
            <h2>Don't have an account?</h2>
          </ion-row>
          <ion-row align-items-center justify-content-around>
            <ion-button color='facebook' href='/register' id='create' fill='clear'>CREATE AN ACCOUNT</ion-button>
          </ion-row>
        </ion-grid>
      </ion-content>

    ];
  }
}


