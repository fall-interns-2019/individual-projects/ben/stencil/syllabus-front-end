import { Component, h } from '@stencil/core';

@Component({
  tag: 'add-assignment',
  styleUrl: 'add-assignment.css'
})
export class AddAssignment {
  assignmentEndPoint = 'http://localhost:3000/api/v1/assignments';
  name: HTMLIonInputElement;
  due_date: HTMLIonInputElement;
  description: HTMLIonInputElement;
  points: HTMLIonInputElement;
  course_id: number;


  sendCourseId() {
    let params = window.location.href.split('/');
    let course_id = params[params.length - 1];
    return (
    <section>
      <ion-button href={`/myassignments/${course_id}`} onClick={() => this.getAddAssignmentInfo()} expand='full' color={'warning'}>ADD ASSIGNMENT</ion-button>
    </section>
    )
  }

  getAddAssignmentInfo() {
    let params = window.location.href.split('/');
    let course_id = params[params.length - 1];
    let assignmentInput = {
      'course_id': course_id,
      'name': this.name.value,
      'due_date': this.due_date.value,
      'description': this.description.value,
      'points': this.points.value
    };
    this.postAssignmentInfo(assignmentInput)
  }

  async postAssignmentInfo(assignmentInput) {
    const res = await fetch(this.assignmentEndPoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(assignmentInput)
    });
    const json = await res.json();
    console.log(json);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Add Assignment</ion-title>
          <ion-buttons slot="primary">
            <ion-button href={'/mycourses'}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
    <ion-content>
      <ion-card>
        <ion-card-content>
        <ion-row style={{paddingTop: '5px'}}align-items-center justify-content-around>
          <ion-avatar style={{marginBottom:'2rem'}}><ion-icon style={{fontSize: '4rem'}} name="contact" color={'indigo'}> </ion-icon></ion-avatar>
        </ion-row>
          <ion-row>
            <ion-label>Assignment Name</ion-label>
          </ion-row>
          <ion-row class="item item-text-wrap">
            <ion-input ref={(el) => this.name = el as HTMLIonInputElement} style={{fontWeight:'bolder', fontSize:'1.2rem'}} placeholder={'Exam 01'} />
          </ion-row>
          <ion-row>
          <ion-label>Due Date</ion-label>
        </ion-row>
          <ion-row>
            <ion-input ref={(el) => this.due_date = el as HTMLIonInputElement} style={{fontWeight:'bolder', fontSize:'1.2rem'}} placeholder={'mm/dd/yy'} />
          </ion-row>
          <ion-row>
            <ion-label>Description</ion-label>
          </ion-row>
          <ion-row>
            <ion-input ref={(el) => this.description = el as HTMLIonInputElement} style={{fontWeight:'bolder', fontSize:'1.2rem'}} placeholder={'Write a 12 page essay'} />
          </ion-row>
          <ion-row>
            <ion-label>Possible points</ion-label>
          </ion-row>
          <ion-row>
            <ion-input ref={(el) => this.points = el as HTMLIonInputElement} style={{fontWeight:'bolder', fontSize:'1.2rem'}} type={'number'} placeholder={'50'} />
          </ion-row>
          {this.sendCourseId()}
        </ion-card-content>
      </ion-card>
    </ion-content>
    ];
  }
}
